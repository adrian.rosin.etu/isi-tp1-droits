import os

euid = os.geteuid()
egid = os.getegid()

print("Effective user ID: ", euid)
print("Effective group ID: ", egid)

