#!/bin/bash

sudo adduser lambda_a
sudo adduser lambda_b

sudo groupadd groupe_a
sudo groupadd groupe_b

sudo adduser lambda_a groupe_a
sudo adduser lambda_b groupe_b

mkdir dir_a
mkdir dir_b
mkdir dir_c

chgrp -R groupe_a dir_a
chgrp -R groupe_b dir_b

chmod g+r dir_a dir_b dir_c
chmod o+r dir_c
chmod o-w dir_c
chmod o-rwx dir_b dir_a
chmod +t dir_a dir_b dir_c

