# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- ROSIN Adrian email: adrian.rosin.etu@univ-lille.fr

- HAVARD Maxime email: maxime.havard.etu@univ-lille.fr

## Question 1

Le processus ne peut pas écrire car l'utilisateur toto ne dispose pas des droits d'écriture mais uniquement des droits de lecture

## Question 2

Le caractère X pour un répertoire permet d'avoir les droits d'accéder au contenu de ce dernier ainsi que de pouvoir modifier, ajouter ou renommer les fichiers qui sont à l'intérieur. 

Avec l'utilisateur Toto, lorsque l'on essaie d'accéder au dossier, le message permission denied s'affiche car l'utilisateur Toto est dans un groupe qui ne possède pas les droits d'exécution sur le dossier mydir. 

Nous n'avons toujours pas accès aux fichiers car toto n'est ni propriétaire ni dans le groupe qui possèdent les permissions d'accès au dossier, et comme dans le troisième triplet de permissions les utilisateur n'ont pas les droits nous avons par conséquent pas l'accès à ce dossier ni à son contenu

## Question 3

Pour ce qui de l'utilisateur ubuntu nous avons les valeurs 1000 et pour l'utilisateur toto nous avons les valeurs 1001, le processus arrive cependant à ouvrir en lecture le fichier mydata.txt.

en activant le flag set-user-id, la valeur de l'euid à changée et est passée a 1000 contrairement a 1001 sans le flag. Le processus arrive toujours à ouvrir en lecture le fichier mydata.txt

## Question 4

En activant le flag set-user-id sur le fichier python et en l'exécutant avec l'utilisateur toto, on obtient la valeur 1001 pour l'EUID et l'EGID.

## Question 5

La commande "chfn" permet de modifier le nom et les informations complémentaires à un utilisateur.
Les informations dans etc/passwd ont été correctement modifiées après l'appel à "chfn".

## Question 6

Les mots de passe utilisateur sont stockés dans "etc/shadow", cela permet aux utilisateur de ne pas avoir accès facilement à ces fichiers. Les mots de passe sont chiffrés par hashage pour renforcer la sécurité car le fichier est tout de même accessible si l'on possède les droits suffisants. 

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








