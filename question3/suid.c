#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    printf("%d\n", geteuid());
    printf("%d\n", getegid());

    printf("%d\n", getgid());
    printf("%d\n", getuid());

    FILE *f;

    if (argc < 2) {
        printf("Missing arguments\n");
        exit(EXIT_FAILURE);
    }
    printf("Hello World\n");
    f = fopen(argv[1], "r");

    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    printf("File opens successfully\n");
    fclose(f);
    exit(EXIT_SUCCESS);
}
