#include <stdio.h>
#include <unistd.h>
#include <stdlib.h> 

int main(int argc, char const *argv[])
{
	if(argc != 2) {
		printf("Missing arguments\n");
		exit(EXIT_FAILURE);
	}
	printf("%s\n", getenv("USER"));

	char *user = getenv("USER");
	char pwd[48];
	printf("Enter your password :");
	fgets(pwd, 48, stdin);

	if (check_password(user, pwd) == 0) {
		rmg(argv[1]);
	} else { 
		printf("Wrong password!");
		exit(EXIT_FAILURE);
	}

}

int check_password(char *user, char *password) {
	FILE *file;
	char *tok;
	const char *separators = ",:/";

	file = fopen("../home/maxime/passwd", "r");
	if (file == NULL) {
		printf("File not found");
		exit(EXIT_FAILURE);
	}

	while(fgets(user, 80, file)) {
		tok = strtok(80, separators);
		while (tok != NULL) {
			if (strcmp(tok, user) == 0){
				tok = strtok(NULL, separators);
				return 0;
			}
		}
	}
	printf("Incorrect Password");
	fclose(file);
	exit(EXIT_FAILURE);
}

void rmg(char fichier) {
	
	int x = remove(fichier);
	if (x != 0){
		printf("Error, file not deleted");
		exit(EXIT_FAILURE);
	}
	printf("Success");
}
